var app = angular.module('app', ['chart.js', 'ng-currency']);

app.controller('AppController', function($scope, $timeout, $filter) {
	var appCtrl = this;

    appCtrl.message = '';
    appCtrl.projectSearchText = '';
    appCtrl.softwareSearchText = '';
    appCtrl.view = 'list';
    appCtrl.currentProject = {};
    appCtrl.currentSoftware = {};

    appCtrl.Initialize = function(){
        //Initialize all variables
        var arrayNames = ['categoryList', 'statusList', 'phaseList', 'resourceList', 'gateList', 'ownerList', 'maintenanceList', 'projectList', 'softwareList'];
        
        angular.forEach(arrayNames, function(arrayName){
            appCtrl.retrieveData(arrayName, function(data){
                appCtrl[arrayName] = data;
                $timeout(function() {}, 0);     
            });
        });
    };
    
    ///Returns the list of names for each of the software's available
    appCtrl.softwareNames = function(){
        return Enumerable.From(appCtrl.softwareList).Select(function(d) { return d.Software}).ToArray();
    }
    
    ///Adds the input software name to the currentProject's list of software
    appCtrl.addSoftware = function(software){
        if (!appCtrl.currentProject.software){
            appCtrl.currentProject.software = [];   
        }
        
        appCtrl.currentProject.software.push(software);
        
        $timeout(function() {}, 0);
    };
    
    //Adds a new software to the tool
    appCtrl.addNewSoftware = function(){
        var newSoftware = {licenses: []};
        appCtrl.softwareList.push(newSoftware);
        appCtrl.setCurrentSoftware(newSoftware);
    };
    
    appCtrl.addSoftwareLicense = function(software){
        appCtrl.currentSoftware.licenses.push({User: "", ExpirationDate: new Date()});
        
        $timeout(function() {}, 0);
    };
    
    ///Returns the date that the next license to expire will expire on
    appCtrl.nextExpiringLicense = function(software){
        var returnDate = false;
        
        software.licenses = Enumerable.From(software.licenses).OrderBy(function(d) { return new Date(Date.parse(d.ExpirationDate))}).ToArray();
        
        angular.forEach(software.licenses, function(license){
            if (!returnDate){
                var tempDate = new Date(Date.parse(license.ExpirationDate));
                
                if (software.licenses.length > 1){
                    if (tempDate < new Date()){
                        license.Message = 'License expired on ';
                    }else{
                        license.Message = 'Next license expiring on ';   
                    }
                }else{
                    if (tempDate < new Date()){
                        license.Message = 'License expired on ';
                    }else{
                        license.Message = 'License expiring on ';   
                    }
                }

                if (tempDate != 'Invalid Date'){
                    returnDate = license;
                }
            }
        });
        
        if (returnDate){
            
            return returnDate;   
        }
    };
    
    appCtrl.resourceNames = function(){
        return Enumerable.From(appCtrl.resourceList).Select(function(d) { return d.Name;}).ToArray();  
    };
    
    appCtrl.retrieveData = function(documentName, callback){
        $.get(
			'http://a10036844:8080/databases/IPM/docs/' + documentName,
			function (data) {
			    callback(data.Data);
			},
			'json'
		);
    };

	appCtrl.setView = function(name){
		appCtrl.view = name;
	};

	appCtrl.setCurrentProject = function(project) {
		appCtrl.currentProject = project;
        
		appCtrl.view = 'project';
	};
    
    appCtrl.setCurrentSoftware = function(software){
        appCtrl.currentSoftware = software;
        
        angular.forEach(appCtrl.currentSoftware.licenses, function(license){
            license.ExpirationDate = new Date(Date.parse(license.ExpirationDate));
        });
        
        appCtrl.setView('software');
    };
    
    appCtrl.viewSoftwareByName = function(softwareName){
        var software = Enumerable.From(appCtrl.softwareList).Where(function(d) { return d.Software == softwareName }).FirstOrDefault();
        if (software){
            appCtrl.setCurrentSoftware(software);
        }else{
            appCtrl.showUserMessage('Error: unable to find software');
        }
    };

	appCtrl.removeProject = function ($index) {
		swal({   
			title: 'Are you sure?',   
			text: 'You will not be able to recover this row!',  
			 type: 'warning',   
			 showCancelButton: true,   
			 confirmButtonColor: '#DD6B55',   
			 confirmButtonText: 'Yes, delete it!',  
			 closeOnConfirm: false 
			}, 
			function(){   
				appCtrl.projectList.splice($index,1);
				$timeout(function(){}, 0);
				appCtrl.writeData();
				swal('Deleted!', 'The row has been deleted.', 'success');
			}
		);
	};

	appCtrl.addProject = function() {
        var newProj = {name: 'New Project', status: 'Not Started', phase: 'Innovation', gate: 'N/A', resources: []};
		appCtrl.projectList.push(newProj);
        appCtrl.setCurrentProject(newProj);
	};

    appCtrl.addResource = function(nameToAdd) {
        appCtrl.currentProject.resources.push({name: nameToAdd, percent: 0});
    };
    
    appCtrl.uploadFile = function(){
        var fileInput = document.getElementById('file');
        if (fileInput.files.length > 0){
            var file = fileInput.files[0];
            
            var reader = new FileReader();
            
            reader.onload= function(e){
                var data = new FormData();
                
                data.append('', file, file.name);
                
                $.ajax({
                    url: 'http://cgacedev1/FileUploader/api/FileUpload/UploadFile/',
                    type: 'POST',
                    data: data,
                    dataType: 'text',
                    cache: false,
                    xhrFields: { withCredentials: true },
                    processData: false,
                    contentType: false,
                    success: function(data){
                        data = JSON.parse(data);
                        appCtrl.showUserMessage('Successfully uploaded ' + file.name);
                        if (!angular.isArray(appCtrl.currentProject.files)){
                            appCtrl.currentProject.files = [];
                        }
                        appCtrl.currentProject.files.push({key: data, name: file.name});
                        appCtrl.writeData();
                    }, error: function(status){
                        appCtrl.showUserMessage('Failed to upload ' + file.name + ', please contact developers.');   
                    }
                });
            }
            
            reader.readAsBinaryString(file);
        }else{
            appCtrl.showUserMessage('Please select a file.');
        }
    };

	appCtrl.writeData = function () {
        $.ajax({
            url: 'http://a10036844:8080/databases/IPM/docs/projectList',
            type: 'PUT',
            data: JSON.stringify({Name: "projectList", Data: appCtrl.projectList}),
            contentType: 'application/json',
            success: function(){
                appCtrl.showUserMessage('Successfully saved updates');
                $timeout(function(){}, 0);//Ensure the UI refreshes
            }
        });
        $.ajax({
            url: 'http://a10036844:8080/databases/IPM/docs/softwareList',
            type: 'PUT',
            data: JSON.stringify({Name: 'softwareList', Data: appCtrl.softwareList}),
            contentType: 'application/json',
            success: function(){
                appCtrl.showUserMessage('Successfully saved updates');
                $timeout(function() {}, 0);
            }
        });
	};
    
    appCtrl.showUserMessage = function(inputMessage){
        appCtrl.message = inputMessage;
        appCtrl.showMessage = true;
        $timeout(function(){}, 0);
        //Separate out the show/clear method for smoother transitions
        $timeout(function(){ appCtrl.showMessage=false; }, 3000);
        $timeout(function(){ appCtrl.message = '';}, 3500);
    }

	appCtrl.getData = function() {
		var result = [0,0,0];
		angular.forEach(appCtrl.projectList, function(project) {
			if(project.phase == 'Innovation') {
				result[0]++;
			}
			if(project.phase == 'Innovation') {
				result[1]++;
			}
			if(project.phase == 'Innovation') {
				result[2]++;
			}
		});

	};

  	appCtrl.data = [0,0,0];

  	appCtrl.getChartData = function() {
		var result = [0,0,0];
		angular.forEach(appCtrl.projectList, function(project) {
			if(project.phase == 'Innovation') {
				result[0]++;
			}
			if(project.phase == 'Commercialization') {
				result[1]++;
			}
			if(project.phase == 'Support') {
				result[2]++;
			}
		});
		appCtrl.data = result;
	};
});

app.filter('softwareSearchFilter', function(){
    return function(softwares, searchString){
        var filtered = [];
        searchString = searchString.toLowerCase();
        var searchArray = searchString.split('+');
        
        angular.forEach(softwares, function(software){
            software.searchResults = [];
            var matches = false;
            
            angular.forEach(searchArray, function(text){
                var valToSearch = software.Software + ' - ' + software.Company;
                var startIndex = valToSearch.toLowerCase().indexOf(text.trim());
                if (startIndex >= 0){
                    matches= true;
                    
                    if (text.length > 0){
                        software.searchResults.push(getSearchResults('', valToSearch, text, startIndex));
                    }
                }
            });
            
            if (matches){
                filtered.push(software); 
            }
        });
        
        return Enumerable.From(filtered).OrderBy(function(d) { return d.Software }).ToArray();
    };
});

app.filter('searchFilter', function () {
	return function (projects, categories, searchString) {
		var filtered = [];
		searchString = searchString.toLowerCase();
		var searchArray = searchString.split('+');
		angular.forEach(projects, function(project) {
            //Clear all filter results
            project.searchResults = [];
            var test = false;

            angular.forEach(categories, function(category){
                angular.forEach(searchArray, function(text) {
                    var valToSearch = "";
                    if(Array.isArray(project[category.value])) {
                        angular.forEach(project[category.value], function(obj) {
                            valToSearch += obj.name + "                  ";
                        });
                    } else {
                        valToSearch = project[category.value];
                    }
                    if (valToSearch != undefined){
                        var startIndex = valToSearch.toLowerCase().indexOf(text.trim());
                        if(startIndex >= 0){
                            test = true;
                            if (text.length > 0){
                                project.searchResults.push(getSearchResults(category.name + ': ', valToSearch, text, startIndex));
                            }
                        }
                    }else{
                        test==true; //Value is null, include it in the search results
                    }
                });
            });
            
            if (test == true) {
                filtered.push(project);
            }
		});
		filtered.sort(function(a, b){ 
			if(a.name.toLowerCase() > b.name.toLowerCase()) {
				return 1;
			} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
				return -1;
			} else {
				return 0;
			}
		});
		return filtered;
	};
});

///Creates a search result object that can be used to display the search results to the user
function getSearchResults(prefix, inputText, searchText, startIndex){
    var outputStart = findSpace(-1, inputText, startIndex - 5);
    var outputEnd = findSpace(1, inputText, startIndex + searchText.length + 5);
    var output = inputText.substring(outputStart, outputEnd);
    var highlightStartIndex = startIndex - outputStart;
    var highlightEndIndex = highlightStartIndex + searchText.length;
    
    return {
        firstFilterResult: (prefix + output.substring(0, highlightStartIndex)),
        highlightedFilterResult: output.substring(highlightStartIndex, highlightEndIndex).trim(),
        lastFilterResult: (output.substring(highlightEndIndex, output.length))
    }
}


///Starts at the startIndex and traverses the input by the interval looking for a space (' ')
///Returns the index of the space, or the beginning/end of the input if none is found
function findSpace(interval, input, startIndex){
    var charLoc = startIndex;
    var foundChar = '';

    while (foundChar != ' ' && charLoc > 0 && charLoc < input.length){
        foundChar = input.substring(charLoc, charLoc + 1);

        if (foundChar != ' '){
          charLoc = charLoc + interval;
        }
    }
    
    if (charLoc < 0){
        charLoc = 0;
    }
    if (charLoc > input.length){
        charLoc = input.length;   
    }

    return charLoc;
}
app.filter('phaseFilter', function () {
	return function (projects, phase) {
		var filtered = [];
		phase = phase.toLowerCase();
		angular.forEach(projects, function(project) {
			if(project.phase.toLowerCase().indexOf(phase) >= 0){
				filtered.push(project);
			}
		});
		return filtered;
	};
});

String.prototype.splice = function(index, rem, s){
    return (this.slice(0,index) + s + this.slice(index + Math.abs(rem)));
}